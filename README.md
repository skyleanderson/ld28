melonJS boilerplate - ska version
-------------------------------------------------------------------------------

Modified from the standard boilerplate for a simpler resources structure and some basic functionality.

-------------------------------------------------------------------------------

features :
- video autoscaling
- mobile optimized HTML/CSS
- swiping disabled on iOS devices
- debug Panel (if #debug)

-------------------------------------------------------------------------------
Copyright (C) 2011 - 2013, Olivier Biot, Jason Oster
melonJS is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php)