/* Game namespace */
var SKA = {
    // Run on page load.
    "onload" : function () {
        // Initialize the video.
        if (!me.video.init("screen", 480, 320, true, 'auto'))
        {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }
		
		// add "#debug" to the URL to enable the debug Panel
		if (document.location.hash === "#debug") {
			window.onReady(function () {
				me.plugin.register.defer(debugPanel, "debug");
			});
		}

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
     
        // Load the resources.
        me.loader.preload(SKA.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },



    // Run on game resources loaded.
    "loaded" : function () {
        me.state.set(me.state.MENU, new SKA.TitleScreen());
        me.state.set(me.state.PLAY, new SKA.PlayScreen());
        me.state.set(me.state.CREDITS, new SKA.InfoScreen('credits_bg', me.state.MENU));
        me.state.set(me.state.USER, new SKA.InfoScreen('level1_bg0', me.state.USER + 1));
        me.state.set(me.state.USER + 1, new SKA.InfoScreen('level1_bg1', me.state.PLAY));
        me.state.set(me.state.GAME_END, new SKA.InfoScreen('game_end', me.state.CREDITS));
        me.state.set(me.state.GAMEOVER, new SKA.InfoScreen('game_over', me.state.CREDITS));

        SKA.bindKeys();

        //pooled entities
        me.entityPool.add("player", SKA.PlayerEntity);
        me.entityPool.add("companion", SKA.CompanionEntity);
        me.entityPool.add("other", SKA.OtherEntity);
        me.entityPool.add("hater", SKA.HaterEntity);
        me.entityPool.add("particle", SKA.ParticleEntity);
        me.entityPool.add("coin", SKA.CollectableEntity);
        me.entityPool.add("panda", SKA.CollectableEntity);
        me.entityPool.add("kitty", SKA.CollectableEntity);

        SKA.font16white = new me.BitmapFont('font32white', 32);
        SKA.font16white.resize(.4);
        SKA.font24white = new me.BitmapFont('font32white', 32);
        SKA.font24white.resize(.7);


        //set high scores
        SKA.scoreStory = 0;
        SKA.scoreStoryHigh = 0;
        SKA.score1 = 0;
        SKA.score1high = 0;
        SKA.score2 = 0;
        SKA.score2high = 0;
        SKA.score3 = 0;
        SKA.score3high = 0;

        SKA.level = 1;

        // Start the game.
        me.debug.renderHitBox = true;
        me.state.transition('fade', '#000000', 500);
        me.state.change(me.state.MENU);
    }
};

SKA.bindKeys = function ()
{
    //input buttons
    me.input.bindKey(me.input.KEY.UP, "up");
    me.input.bindKey(me.input.KEY.DOWN, "down");
    me.input.bindKey(me.input.KEY.LEFT, "left");
    me.input.bindKey(me.input.KEY.RIGHT, "right");
    me.input.bindKey(me.input.KEY.ENTER, "enter", true);
    me.input.bindKey(me.input.KEY.ESCAPE, "escape", true);
}

SKA.unbindKeys = function ()
{
    //input buttons
    me.input.unbindKey(me.input.KEY.UP); // "up");
    me.input.unbindKey(me.input.KEY.DOWN); // "down");
    me.input.unbindKey(me.input.KEY.LEFT); // "left");
    me.input.unbindKey(me.input.KEY.RIGHT); // "right");
    me.input.unbindKey(me.input.KEY.ENTER); // "enter"); // true);
    me.input.unbindKey(me.input.KEY.ESCAPE); // "escape"); // true);
}
