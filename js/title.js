SKA = SKA || {};

SKA.TitleScreen = me.ScreenObject.extend(
{
    /*
    * constructor
    */
    init: function ()
    {
        // call parent constructor
        this.parent(true, true);

        // init stuff
        this.background = null;
        this.play = null;
        this.credits = null;
        this.version = null;
        this.selection = null;
        
        SKA.titleIndex = 0;
    },


    /*
    * reset function
    */
    onResetEvent: function ()
    {

        //change keys
        SKA.unbindKeys();
        me.input.bindKey(me.input.KEY.UP, 'up', true); 
        me.input.bindKey(me.input.KEY.DOWN, 'down', true);
        me.input.bindKey(me.input.KEY.ENTER, 'enter', true);

        // load title image
        this.background = me.loader.getImage("title_bg");
        this.selection = me.loader.getImage("title_select");

        // play button
        this.play = new SKA.Button(0, "play", me.state.USER, 152, 102);
        this.sa1 = new SKA.Button(1, "sa1", me.state.PLAY, 152, 137);
        this.sa2 = new SKA.Button(2, "sa2", me.state.PLAY, 152, 172);
        this.sa3 = new SKA.Button(3, "sa3", me.state.PLAY, 152, 207);
        this.credits = new SKA.Button(4, "credits", me.state.CREDITS, 152, 242);

        //            this.mute = new MuteButton(me.video.getWidth() - 35, me.video.getHeight() - 35); 

        // version
        this.version = new me.Font("'Arial Black', Gadget, sans-serif", 18, "#dddddd");;
        me.game.sort();
    },

    /**
     * update function
     */
    update: function ()
    {
        this.parent();

        if (me.input.isKeyPressed("down"))
        {
            SKA.titleIndex += 1;
        }
        else if (me.input.isKeyPressed("up"))
        {
            SKA.titleIndex -= 1;
        }

        if (SKA.titleIndex < 0) SKA.titleIndex = 0;
        else if (SKA.titleIndex > 4) SKA.titleIndex = 4;


        //check if the mouse is hovered
        if (this.play.containsPoint(me.input.mouse.pos))
        {
            SKA.titleIndex = 0;
        } else if (this.play.containsPoint(me.input.mouse.pos))
        {
            SKA.titleIndex = 1;
        }

        if (me.input.isKeyPressed("enter"))
        {
            if (SKA.titleIndex === 0)
            {
                SKA.storyMode = true;
                SKA.level = 1;
            } else
            {
                SKA.storyMode = false;
                SKA.level = SKA.titleIndex;
            }

            if (SKA.titleIndex  === 0)
                me.state.change(me.state.USER);
            else if (SKA.titleIndex < 4)
                me.state.change(me.state.PLAY);
            else
                me.state.change(me.state.CREDITS);

        }
        return true;
    },

    /*
    * drawing function
    */
    draw: function (context)
    {
        // draw title
        context.drawImage(this.background, 0, 0, me.video.getWidth(), me.video.getHeight());

        // draw play button
        this.play.draw(context);
        this.sa1.draw(context);
        this.sa2.draw(context);
        this.sa3.draw(context);
        this.credits.draw(context);

        //draw selection indicator
        context.drawImage(this.selection, 100, this.play.pos.y + 4 + (35 * SKA.titleIndex));

        //high scores
        if (SKA.titleIndex < 4)
        {
            if (SKA.titleIndex === 0 && SKA.scoreStoryHigh)
            {
                SKA.font24white.set('left');
                SKA.font24white.draw(context, "High:", 40, 70);
                SKA.font24white.set('right');

                SKA.font24white.draw(context, "" + SKA.scoreStoryHigh, 432, 70);

                SKA.font24white.set('left');
            } else if (SKA['score' + SKA.titleIndex + 'high'])
            {
                SKA.font24white.set('left');
                SKA.font24white.draw(context, "High:", 40, 70);
                SKA.font24white.set('right');
                SKA.font24white.draw(context, "" + SKA['score' + SKA.titleIndex + 'high'], 432, 70);
                SKA.font24white.set('left');
            }

        }

        //            this.mute.draw(context);
        var versionText = "LD 28 #skyleanderson";
        SKA.font16white.set('right');
        SKA.font16white.draw(context, versionText,
            me.video.getWidth() - 5, me.video.getHeight() - 21);
        SKA.font16white.set('left');
    },

    /*
    * destroy event function
    */
    onDestroyEvent: function ()
    {
        // release mouse event
        SKA.unbindKeys();
        SKA.bindKeys();
        me.input.releasePointerEvent("mousedown", this.play);
        me.input.releasePointerEvent("mousedown", this.sa1);
        me.input.releasePointerEvent("mousedown", this.sa2);
        me.input.releasePointerEvent("mousedown", this.sa3);
        me.input.releasePointerEvent("mousedown", this.credits);
    }
});

/*
* draw a button on screen. Borrowed from invasionJS by semche
*/
SKA.Button = me.Rect.extend(
{
    /*
    * constructor
    */
    init: function (index, image, action, x, y)
    {
        // init stuff
        this.index = index;
        this.image = me.loader.getImage(image);
        this.action = action;

        //if x or y is -1, center on screen
        var endx = x >= 0 ? x : (me.video.getWidth() / 2 - this.image.width / 2);
        var endy = y >= 0 ? y : (me.video.getHeight() / 2 - this.image.height / 2);

        this.pos = new me.Vector2d(endx, endy);

        // call parent constructor
        this.parent(this.pos, this.image.width, this.image.height);

        // register mouse event
        me.input.registerPointerEvent("mousedown", this, this.clicked.bind(this));
    },

    /*
    * action to perform when a button is clicked
    */
    clicked: function ()
    {
        // start action
        if (this.index === 0)
        {
            SKA.storyMode = true;
            SKA.level = 1;
        } else
        {
            SKA.storyMode = false;
            SKA.level = this.index;
        }
        me.state.change(this.action);
    },

    /*
    * drawing function
    */
    draw: function (context)
    {
        context.drawImage(this.image, this.pos.x, this.pos.y);
    },

    update: function ()
    {

        this.parent();
        return true;
    },

    /*
    * destroy event function
    */
    onDestroyEvent: function ()
    {
        // release mouse events
        me.input.releasePointerEvent("mousedown", this);
    }
});
