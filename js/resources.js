SKA = SKA || {};

SKA.resources = [

	/* Graphics. 
	 * @example
	 * {name: "example", type:"image", src: "res/img/example.png"},
	 */
	
    //Screen elements
    { name: "title_bg",     type: "image", src: "res/img/title_bg.png" },
    { name: "play",         type: "image", src: "res/img/story.png" },
    { name: "sa1", type: "image", src: "res/img/sa1.png" },
    { name: "sa2", type: "image", src: "res/img/sa2.png" },
    { name: "sa3", type: "image", src: "res/img/sa3.png" },
    { name: "credits", type: "image", src: "res/img/credits.png" },
    { name: "title_select", type: "image", src: "res/img/title_select.png" },
    { name: "credits_bg", type: "image", src: "res/img/credits_bg.png" },
    { name: "level1_bg0", type: "image", src: "res/img/level1_bg0.png" },
    { name: "level1_bg1", type: "image", src: "res/img/level1_bg1.png" },
    { name: "game_end", type: "image", src: "res/img/game_end.png" },
    { name: "game_over", type: "image", src: "res/img/game_over.png" },
    { name: "font32white", type: "image", src: "res/img/font32white.png" },

    //game objects
    { name: "guy", type: "image", src: "res/img/guy.png" },
    { name: "gal", type: "image", src: "res/img/gal.png" },
    { name: "other", type: "image", src: "res/img/other.png" },
    { name: "hater", type: "image", src: "res/img/hater.png" },
    { name: "particle", type: "image", src: "res/img/guy.png" },
    { name: "coin", type: "image", src: "res/img/coin.png" },
    { name: "coin_icon", type: "image", src: "res/img/coin_icon.png" },
    { name: "panda", type: "image", src: "res/img/panda.png" },
    { name: "kitty", type: "image", src: "res/img/kitty.png" },
    { name: "clock", type: "image", src: "res/img/clock.png" },

    //map stuff
    { name: "tile", type: "image", src: "res/img/tile.png" },
    { name: "metatiles16x16", type: "image", src: "res/img/metatiles16x16.png" },


	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "res/map/example01.tmx"},
 	 */
    { name: "level1", type: "tmx", src: "res/level1.tmx" },
    { name: "level2", type: "tmx", src: "res/level2.tmx" },
    { name: "level3", type: "tmx", src: "res/level3.tmx" },

	/* Background music. 
	 * @example
	 * {name: "example_bgm", type: "audio", src: "res/sfx/", channel : 1},
	 */	
	
	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "res/sfx/", channel : 2}
	 */
     { name: "bump", type: "audio", src: "res/sfx/", channel: 1 },
     { name: "pickup", type: "audio", src: "res/sfx/", channel: 1 },
     { name: "hurt", type: "audio", src: "res/sfx/", channel: 3 },
     { name: "safe", type: "audio", src: "res/sfx/", channel: 2 },
     { name: "hater", type: "audio", src: "res/sfx/", channel: 2 },
];
