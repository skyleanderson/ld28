﻿SKA = SKA || {};

SKA.InfoScreen = me.ScreenObject.extend(
{
    init: function (image, nextState)
    {
        this.parent(true, true);
        this.background = null;
        this.image = image;
        this.nextState = nextState;
        SKA.screen = new me.Rect({x: 0, y:0}, me.video.getWidth(), me.video.getHeight());
    },

    /**	
        *  action to perform on state change
        */
    onResetEvent: function ()
    {

        this.background = me.loader.getImage(this.image);
        SKA.nextState = this.nextState;
        me.input.registerPointerEvent('mouseup', SKA.screen, this.mouseUpHandler);

    },

    update: function ()
    {
        this.parent();
        if (me.input.isKeyPressed("enter") || me.input.isKeyPressed("escape"))
            me.state.change(SKA.nextState);

        return true;
    },

    draw: function (context)
    {
        context.drawImage(this.background, 0, 0, me.video.getWidth(), me.video.getHeight());
    },

    mouseUpHandler: function ()
    {
        me.state.change(SKA.nextState);
        me.input.releasePointerEvent('mouseup', SKA.screen, this.mouseUpHandler);
    }
});