SKA = SKA || {};

SKA.PlayScreen = me.ScreenObject.extend({
	/**	
	 *  action to perform on state change
	 */
    onResetEvent: function ()
    {
        SKA.enemyList = [];
        SKA.collectableCount = 0;
	    me.levelDirector.loadLevel("level" + SKA.level);
	},
	
	
	/**	
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() {
	  ; // TODO
	}
});
