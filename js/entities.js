SKA = SKA || {};

SKA.CONST = {
    PLAYER: {
        SPEED: 1.5,
        PARTICLE_COOLDOWN: 200,
        MAX_A: .5,
        MIN_A: .1,
        BOUNCE: 16,
        DAMAGE_COOLDOWN: 200,
        PICKUP_COOLDOWN: 2000
    },

    COMPANION: {
        SPEED: .2,
        STAND_COOLDOWN: 900,
        WALK_COOLDOWN: 6000,
        PARTICLE_COOLDOWN: 200,
        MAX_RADIUS: 100,
        MAX_ROTATION: Math.PI / 1000,
    },

    OTHER: {
        SPEED: .8,
        WALK_COOLDOWN: 8000,
        STAND_COOLDOWN: 300,
        MAX_ROTATION: Math.PI / 2200,
        FRIEND_COOLDOWN: 2000
    },

    HATER: {
        SPEED: 2.3,
        WALK_COOLDOWN: 1400,
        STAND_COOLDOWN: 8000,
        FRIEND_COOLDOWN: 500
    },

    PARTICLE: {
        FOLLOW_SPEED: 1
    },

    LAYERS: {
        PARTICLES: 3,
        PLAYER: 4,
        COMPANION: 5,
    },

    POINTS: {
        FIRST_FRIEND: 400,
        FRIEND: 75,
    }
};

////////////////////////////////////////////////////////////////////////////////////
//        PLAYER ENTITY             ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
SKA.PlayerEntity = me.ObjectEntity.extend(
{

    init: function (x, y, settings)
    {
        //timing
        SKA.lastTime = new Date().getTime();
        SKA.currTime = null;
        SKA.millis = 0;

        // call the constructor
        settings.image = "guy";
        settings.spritewidth = 16;
        settings.spriteheight = 16;
        this.parent(x, y, settings);
        //this.renderable.resize(2);

        // set the walking & jumping speed
        //set to large number since i'll handle it
        this.setVelocity(100, 100);

        // adjust the bounding box
        this.updateColRect(2, 12, 4, 12);

        // top-down, no gravity
        this.collidable = true;
        this.jumping = false;
        this.falling = false;
        this.gravity = 0;
        this.alwaysUpdate = true;
        this.facingRight = true;
        this.facingDown = true;
        this.particleCooldown = SKA.CONST.PLAYER.PARTICLE_COOLDOWN;
        this.particle = null;
        this.a = .3;
        this.isMouseDown = false;
        me.input.registerPointerEvent('mousedown', me.game.viewport, this.mouseDownHandler);
        me.input.registerPointerEvent('mouseup', me.game.viewport, this.mouseUpHandler);
        this.res = null;
        this.damageCooldown = 0;
        this.index = 0;
        this.collision = false;
        this.score = 0;

        this.clock = me.loader.getImage('clock');
        this.coin = me.loader.getImage('coin_icon');
        this.panda = me.loader.getImage('panda');
        this.kitty = me.loader.getImage('kitty');

        this.pickup = null;
        this.pickupLabel = "";
        this.pickupCooldown = 0;

        //add this object to the global namespace so others can reference it
        SKA.player = null;
        SKA.player = this;

        me.game.sort();

        this.renderable.addAnimation("stand-d", [0]);
        this.renderable.addAnimation("stand-u", [2]);
        this.renderable.addAnimation("walk-d", [0, 1]);
        this.renderable.addAnimation("walk-u", [2, 3]);

        SKA.levelTime = 120000;
        SKA.gameover = false;
        
    },

    setAnimation: function (animation, restart)
    {
        if (restart)
        {
            //just set animation, this will restart it
            this.renderable.setCurrentAnimation(animation, animation);
            this.renderable.setAnimationFrame();
        } else
        {
            //only set if it is a different animation
            if (!this.renderable.isCurrentAnimation(animation))
            {
                this.renderable.setCurrentAnimation(animation, animation);
                this.renderable.setAnimationFrame();
            }
        }
    },

    mouseDownHandler: function(event)
    {
        SKA.player.isMouseDown = true;
    },

    mouseUpHandler: function(event)
    {
        SKA.player.isMouseDown = false;
    },

    addScore: function(amount)
    {
        this.score += amount;
        if (SKA.storyMode)
        {
            SKA.scoreStory += amount;
            if (SKA.scoreStory > SKA.scoreStoryHigh) SKA.scoreStoryHigh = SKA.scoreStory;
        }
        //else
        {
            SKA['score' + SKA.level] = this.score;
            if (SKA['score' + SKA.level] > SKA['score' + SKA.level + 'high']) SKA['score' + SKA.level + 'high'] = SKA['score' + SKA.level];
        }

    },

    setPickup: function(pickup, label)
    {
        this.pickup = pickup;
        this.pickupLabel = label;
        this.pickupCooldown = SKA.CONST.PLAYER.PICKUP_COOLDOWN;
    },

    checkGameOver: function()
    {
        var allFriends = true;
        if (SKA.levelTime < 0)
        {
            //level done
            this.finishLevel.call(this);
            return true;
        }

        for (this.index = 0; this.index < SKA.enemyList.length; this.index++)
        {
            if (!SKA.enemyList[this.index].friend)
            {
                allFriends = false;
            }
        }
        if (allFriends && SKA.collectableCount === 0)
        {
            //level done
            this.finishLevel.call(this);
            return true;
        }
        return false;
    },

    finishLevel: function()
    {
        if (!SKA.gameover)
        {
            if (SKA.storyMode && SKA.level < 3)
            {
                //go to next level
                SKA.level += 1;
                me.state.change(me.state.PLAY);
            } else if (SKA.storyMode && SKA.level >= 3)
            {
                //you win!!!
                me.state.change(me.state.GAME_END);
            } else
            {
                //go to end screen
                me.state.change(me.state.MENU);
            }
            SKA.gameover = true;
        }
    },

    update: function ()
    {
        //timing
        SKA.currTime = new Date().getTime();
        SKA.millis = SKA.currTime - SKA.lastTime;
        if (SKA.millis > 500) SKA.millis = 16;
        SKA.frames = Math.ceil(SKA.millis / 16);
        SKA.levelTime -= SKA.millis;

        if (this.checkGameOver.call(this)) return true;

        if (this.pickupCooldown) this.pickupCooldown -= SKA.millis;

        //input
        if (this.damageCooldown <= 0)
        {
            if (me.input.isKeyPressed('up'))
            {
                this.vel.y = -SKA.CONST.PLAYER.SPEED;
                this.facingDown = false;
            } else if (me.input.isKeyPressed('down'))
            {
                this.vel.y = SKA.CONST.PLAYER.SPEED;
                this.facingDown = true;
            } else
            {
                this.vel.y = 0;
                this.facingDown = true;
            }

            if (me.input.isKeyPressed('left'))
            {
                this.vel.x = -SKA.CONST.PLAYER.SPEED;
                this.facingRight = false;
            } else if (me.input.isKeyPressed('right'))
            {
                this.vel.x = SKA.CONST.PLAYER.SPEED;
                this.facingRight = true;
            } else
            {
                this.vel.x = 0;
            }
        } else
        {
            this.vel.x = 0;
            this.vel.y = 0;
        }


        if (this.isMouseDown)
        {
            if (this.pos.distance(me.input.mouse.pos) > SKA.CONST.PLAYER.SPEED)
            {
                this.vel.x = me.input.mouse.pos.x - this.pos.x;
                this.vel.y = me.input.mouse.pos.y - this.pos.y;
                
                this.facingRight = (this.vel.x >= 0);
                this.facingDown = (this.vel.y >= 0);
            }
        }

        //scale velocity by speed
        this.vel.normalize();
        this.vel.x *= SKA.CONST.PLAYER.SPEED * SKA.frames;
        this.vel.y *= SKA.CONST.PLAYER.SPEED * SKA.frames;

        //update movement
        this.updateMovement();

        //set animation
        if (this.vel.x !== 0 || this.vel.y !== 0)
        {
            //set to moving
            if (this.facingDown)
            {
                this.setAnimation('walk-d');
            } else
            {
                this.setAnimation('walk-u');
            }
        } else
        {
            //set to stand
            if (this.facingDown)
            {
                this.setAnimation('stand-d');
            } else
            {
                this.setAnimation('stand-u');
            }
        }

        this.flipX(!this.facingRight);

        //particles
        this.particleCooldown -= SKA.millis;
        if (this.particleCooldown < 0)
        {
            //spawn particle
            this.particle = me.entityPool.newInstanceOf('particle', this.pos.x + this.hWidth, this.pos.y + this.hHeight,
                {source: this, follow: SKA.companion, image: 'particle', spritewidth: 6, spriteheight: 6, r:200, g:0, b:0});
            me.game.add(this.particle, SKA.CONST.LAYERS.PARTICLES);
            me.game.sort();
            this.particleCooldown += SKA.CONST.PLAYER.PARTICLE_COOLDOWN;
        }


        //check for collisions with enemies
        if (this.damageCooldown > 0) this.damageCooldown -= SKA.millis;
        this.res = null;
        this.res = me.game.collide(this);
        if (this.res)
        {
            if (this.res.obj.type === me.game.ENEMY_OBJECT && this.damageCooldown <= 0)
            {
                //collision, get bumped back
                this.vel.x = this.pos.x - this.res.obj.pos.x;
                this.vel.y = this.pos.y - this.res.obj.pos.y;
                this.vel.normalize();
                this.vel.x *= SKA.CONST.PLAYER.BOUNCE;
                this.vel.y *= SKA.CONST.PLAYER.BOUNCE;
                this.updateMovement();
                this.damageCooldown = SKA.CONST.PLAYER.DAMAGE_COOLDOWN;
                me.game.viewport.shake(3, SKA.CONST.PLAYER.DAMAGE_COOLDOWN);
                me.audio.play('bump');
            }
        }

        //check for collisions with the line
        for (this.index = 0; this.index < SKA.enemyList.length; this.index += 1)
        {
            this.collision = false;
            //check the line vs the 4 edges of the enemy bounding box
            if (SKA.checkLineSegmentCollision(this.pos.x + this.hWidth, this.pos.y + this.hHeight,
                SKA.companion.pos.x + SKA.companion.hWidth, SKA.companion.pos.y + SKA.companion.hHeight,
                SKA.enemyList[this.index].pos.x, SKA.enemyList[this.index].pos.y,
                SKA.enemyList[this.index].pos.x + SKA.enemyList[this.index].width, SKA.enemyList[this.index].pos.y))
            {
                //top
                this.collision = true;
            } else if (SKA.checkLineSegmentCollision(this.pos.x + this.hWidth, this.pos.y + this.hHeight,
                SKA.companion.pos.x + SKA.companion.hWidth, SKA.companion.pos.y + SKA.companion.hHeight,
                SKA.enemyList[this.index].pos.x, SKA.enemyList[this.index].pos.y + SKA.enemyList[this.index].height,
                SKA.enemyList[this.index].pos.x + SKA.enemyList[this.index].width, SKA.enemyList[this.index].pos.y + SKA.enemyList[this.index].height))
            {
                //bottom
                this.collision = true;
            } else if (SKA.checkLineSegmentCollision(this.pos.x + this.hWidth, this.pos.y + this.hHeight,
                SKA.companion.pos.x + SKA.companion.hWidth, SKA.companion.pos.y + SKA.companion.hHeight,
                SKA.enemyList[this.index].pos.x + SKA.enemyList[this.index].width, SKA.enemyList[this.index].pos.y,
                SKA.enemyList[this.index].pos.x + SKA.enemyList[this.index].width, SKA.enemyList[this.index].pos.y + SKA.enemyList[this.index].height))
            {
                //right
                this.collision = true;
            } else if (SKA.checkLineSegmentCollision(this.pos.x + this.hWidth, this.pos.y + this.hHeight,
                SKA.companion.pos.x + SKA.companion.hWidth, SKA.companion.pos.y + SKA.companion.hHeight,
                SKA.enemyList[this.index].pos.x, SKA.enemyList[this.index].pos.y,
                SKA.enemyList[this.index].pos.x, SKA.enemyList[this.index].pos.y + SKA.enemyList[this.index].height))
            {
                //left
                this.collision = true;
            }

            //handle collisions
            if (this.collision && SKA.enemyList[this.index].readyToFriend())
            {
                //if they are close, make friends
                if (this.pos.distance(SKA.companion.pos) <= SKA.CONST.COMPANION.MAX_RADIUS + 2*this.width)
                {
                    me.audio.play('safe');
                    if (!SKA.enemyList[this.index].friend)
                    {
                        SKA.enemyList[this.index].addFriend();
                        this.addScore(SKA.CONST.POINTS.FIRST_FRIEND);
                    } else
                    {
                        //already friended
                        SKA.enemyList[this.index].addFriend();
                        this.addScore(SKA.CONST.POINTS.FRIEND);
                    }
                } else
                {
                    //far away :( game over
                    SKA.enemyList[this.index].addFriend();
                    me.audio.play('hurt');
                    me.state.change(me.state.GAMEOVER);
                }
            }
        }

        this.parent(this);
        SKA.lastTime = SKA.currTime;
        return true;

    },

    draw: function (context)
    {
        //UI
        context.fillStyle = "rgba(0,0,0, .5)";
        context.fillRect(0, 0, me.video.getWidth(), 16);

        if (this.pickupCooldown > 0)
        {
            //draw pickup info
            context.fillRect(0, 304, me.video.getWidth(), 16);
            context.drawImage(this[this.pickup], 120, 304);
            SKA.font16white.set('left');
            SKA.font16white.draw(context, "" + this.pickupLabel, 160, 306);
        }

        //time
        context.drawImage(this.clock, 200, 0);
        SKA.font16white.set('left');
        SKA.font16white.draw(context, "" + Math.ceil(SKA.levelTime / 1000), 240, 2);

        //score
        SKA.font16white.set('right');
        SKA.font16white.draw(context, "" + this.score, 112, 2);
        //highscore
        if (SKA.storyMode)
        {
            SKA.font16white.draw(context, "" + SKA.scoreStoryHigh, 480, 2);
        } else
        {
            SKA.font16white.draw(context, "" + SKA['score' + SKA.level], 480, 2);
        }
        SKA.font16white.set('left');

        //draw connecting line first
        if (SKA.companion)
        {
            context.beginPath();
            //this.a = .3 + .15 * Math.sin(SKA.currTime / 1000);
            this.a = SKA.CONST.PLAYER.MIN_A + (SKA.CONST.PLAYER.MAX_A - SKA.CONST.PLAYER.MIN_A) * (1 - Math.max(0, Math.min(1, (this.pos.distance(SKA.companion.pos) / SKA.CONST.COMPANION.MAX_RADIUS ))));
            context.strokeStyle = "rgba(150, 0, 0," + this.a + ")";
            context.lineWidth = 2;
            context.moveTo(this.pos.x + this.hWidth + 3, this.pos.y + 3 + this.hHeight);
            context.lineTo(SKA.companion.pos.x + SKA.companion.hWidth, SKA.companion.pos.y + SKA.companion.hHeight);
            context.closePath();
            context.stroke();
        }
        this.parent(context);
    },

    destroy: function ()
    {
        me.input.releasePointerEvent('mousedown', me.game.viewport);
        me.input.releasePointerEvent('mouseup', me.game.viewport);
    }
});

////////////////////////////////////////////////////////////////////////////////////
//     COMPANION ENTITY             ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
SKA.CompanionEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "gal";
        settings.spritewidth = 16;
        settings.spriteheight = 16;
        this.parent(x, y, settings);
        //this.renderable.resize(2);

        // set the walking & jumping speed
        //set to large number since i'll handle it
        this.setVelocity(100, 100);

        // adjust the bounding box
        //		this.updateColRect(8, 16, 0, 32);

        // top-down, no gravity
        this.jumping = false;
        this.falling = false;
        this.gravity = 0;
        this.alwaysUpdate = true;

        //states
        this.standCooldown = SKA.CONST.COMPANION.STAND_COOLDOWN;
        this.walkCooldown = 0;
        this.angle = 0;
        this.facingRight = true;
        this.facingDown = true;
        this.particleCooldown = SKA.CONST.COMPANION.PARTICLE_COOLDOWN;
        this.particle = null;
        this.clockwise = settings.clockwise;
        this.points = [];
        this.points[0] = new me.Vector2d(x, y);
        this.points[1] = new me.Vector2d(x + settings.width, y);
        this.points[2] = new me.Vector2d(x + settings.width, y + settings.height);
        this.points[3] = new me.Vector2d(x, y + settings.height);
        this.points[4] = new me.Vector2d(x + .5*settings.width, y + .5 * settings.height);
        this.point = 0;

        SKA.companion = null;
        SKA.companion = this;

        //animations
        this.renderable.addAnimation("stand-d", [0]);
        this.renderable.addAnimation("stand-u", [2]);
        this.renderable.addAnimation("walk-d", [0, 1]);
        this.renderable.addAnimation("walk-u", [2, 3]);
    },


    setAnimation: function (animation, restart)
    {
        if (restart)
        {
            //just set animation, this will restart it
            this.renderable.setCurrentAnimation(animation, animation);
            this.renderable.setAnimationFrame();
        } else
        {
            //only set if it is a different animation
            if (!this.renderable.isCurrentAnimation(animation))
            {
                this.renderable.setCurrentAnimation(animation, animation);
                this.renderable.setAnimationFrame();
            }
        }
    },

    update: function ()
    {
        if (this.standCooldown > 0)
        {
            //standing
            this.standCooldown -= SKA.millis;
            this.vel.x = 0;
            this.vel.y = 0;
            if (this.standCooldown < 0)
            {
                //start walking
                this.walkCooldown = SKA.CONST.COMPANION.WALK_COOLDOWN;
                //pick the next point to walk to
                if (this.clockwise) this.point += 1;
                else this.point -= 1;
                
                if (this.point >= 5) this.point = 0;
                else if (this.point <= -1) this.point = 4;
                
                this.vel.x = this.points[this.point].x - this.pos.x;
                this.vel.y = this.points[this.point].y - this.pos.y;
                this.vel.normalize();
                this.vel.x *= SKA.CONST.COMPANION.SPEED * SKA.frames;
                this.vel.y *= SKA.CONST.COMPANION.SPEED * SKA.frames;
                
                //set animation variables
                if (this.vel.x >= 0)
                {
                    this.facingRight = true;
                } else
                {
                    this.facingRight = false;
                }

                if (this.vel.y <= 0)
                {
                    this.facingDown = false;
                } else 
                {
                    this.facingDown = true;
                }

            }
        } else
        {
            //walking
            //this.walkCooldown -= SKA.millis;
            //wander
            this.vel.x = this.points[this.point].x - this.pos.x;
            this.vel.y = this.points[this.point].y - this.pos.y;
            this.vel.normalize();
            this.vel.x *= SKA.CONST.COMPANION.SPEED * SKA.frames;
            this.vel.y *= SKA.CONST.COMPANION.SPEED * SKA.frames;

            //set animation variables
            if (this.vel.x >= 0)
            {
                this.facingRight = true;
            } else
            {
                this.facingRight = false;
            }

            if (this.vel.y <= 0)
            {
                this.facingDown = false;
            } else
            {
                this.facingDown = true;
            }


            if (this.pos.distance(this.points[this.point]) < SKA.CONST.COMPANION.SPEED) this.standCooldown = SKA.CONST.COMPANION.STAND_COOLDOWN;
        }

        //update movement
        this.updateMovement();

        //set animation
        if (this.vel.x !== 0 || this.vel.y !== 0)
        {
            //set to moving
            if (this.facingDown)
            {
                this.setAnimation('walk-d');
            } else
            {
                this.setAnimation('walk-u');
            }
        } else
        {
            //set to stand
            //if (this.facingDown)
            //{
                this.setAnimation('stand-d');
            //}
            //else
            //{
            //    this.setAnimation('stand-u');
            //}
        }

        this.flipX(!this.facingRight);


        //particles
        this.particleCooldown -= SKA.millis;
        if (this.particleCooldown < 0)
        {
            //spawn particle
            this.particle = me.entityPool.newInstanceOf('particle', this.pos.x + this.hWidth, this.pos.y + this.hHeight,
                { source: this, follow: SKA.player, image: 'particle', spritewidth: 6, spriteheight: 6, r: 200, g: 0, b: 0 });
            me.game.add(this.particle, SKA.CONST.LAYERS.PARTICLES);
            me.game.sort();
            this.particleCooldown += SKA.CONST.COMPANION.PARTICLE_COOLDOWN;
        }
        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        //draw radius line first
        context.beginPath();
        context.strokeStyle = "rgba(150, 0, 0, .2)";
        context.lineWidth = 1;
        //context.moveTo(this.pos.x + this.hWidth + 3, this.pos.y + 3 + this.hHeight);
        context.arc(this.pos.x + this.hWidth, this.pos.y + this.hHeight, SKA.CONST.COMPANION.MAX_RADIUS, 0, 2 * Math.PI);
        context.closePath();
        context.stroke();
        this.parent(context);
    }
});


////////////////////////////////////////////////////////////////////////////////////
//     OTHER ENTITY                 ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
SKA.OtherEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "other";
        settings.spritewidth = 16;
        settings.spriteheight = 16;
        this.parent(x, y, settings);
        this.type = me.game.ENEMY_OBJECT;
        //this.renderable.resize(2);

        // set the walking & jumping speed
        //set to large number since i'll handle it
        this.setVelocity(100, 100);

        // adjust the bounding box
        //		this.updateColRect(8, 16, 0, 32);

        // top-down, no gravity
        this.collidable = true;
        this.jumping = false;
        this.falling = false;
        this.gravity = 0;
        this.alwaysUpdate = true;

        //states
        this.standCooldown = (.9 + .2*Math.random()) * SKA.CONST.OTHER.STAND_COOLDOWN;
        this.walkCooldown = 0;
        this.angle = 0;
        this.facingRight = true;
        this.facingDown = true;
        this.clockwise = false;
        this.friend = false;
        this.friendCooldown = 0;

        //animations
        this.renderable.addAnimation("stand-d", [0]);
        this.renderable.addAnimation("stand-u", [2]);
        this.renderable.addAnimation("walk-d", [0, 1]);
        this.renderable.addAnimation("walk-u", [2, 3]);
        SKA.enemyList.push(this);
    },


    setAnimation: function (animation, restart)
    {
        if (restart)
        {
            //just set animation, this will restart it
            this.renderable.setCurrentAnimation(animation, animation);
            this.renderable.setAnimationFrame();
        } else
        {
            //only set if it is a different animation
            if (!this.renderable.isCurrentAnimation(animation))
            {
                this.renderable.setCurrentAnimation(animation, animation);
                this.renderable.setAnimationFrame();
            }
        }
    },

    addFriend: function()
    {
        this.friend = true;
        this.friendCooldown = SKA.CONST.OTHER.FRIEND_COOLDOWN;
    },

    readyToFriend: function()
    {
        return (this.friendCooldown <= 0);
    },

    update: function ()
    {
        if (this.friendCooldown > 0)
        {
            this.friendCooldown -= SKA.millis;
            this.setAnimation('walk-d');
            this.vel.x = 0;
            this.vel.y = 0;
            this.parent(this);
            return true;
        }

        if (this.standCooldown > 0)
        {
            //standing
            this.standCooldown -= SKA.millis;
            this.vel.x = 0;
            this.vel.y = 0;
            if (this.standCooldown < 0)
            {
                //start walking
                this.walkCooldown = (.9 + .2 * Math.random()) * SKA.CONST.COMPANION.WALK_COOLDOWN;
                //choose angle based on position (walk away from walls)
                if (this.pos.y < 96)
                {
                    //near top
                    if (this.pos.x < 96)
                    {
                        this.angle = 1.5 * Math.PI + Math.random() * .5 * Math.PI; // between 1.5pi and 2pi
                    } else if (this.pos.x > 384)
                    {
                        this.angle = Math.PI + Math.random() * .5 * Math.PI; // between pi and 1.5pi 
                    } else
                    {
                        this.angle = Math.PI + Math.random() * Math.PI; //betweeen pi and 2pi
                    }
                } else if (this.pos.y > 224)
                {
                    //near bot
                    if (this.pos.x < 96)
                    {
                        this.angle = Math.random() * .5 * Math.PI; // between 0 and .5pi 
                    } else if (this.pos.x > 384)
                    {
                        this.angle = .5 * Math.PI + Math.random() * .5 * Math.PI; // between .5pi and pi 
                    } else
                    {
                        this.angle = Math.random() * Math.PI; //betweeen 0 and pi 
                    }
                } else
                {
                    //in middle y
                    if (this.pos.x < 96)
                    {
                        this.angle = 1.5 * Math.PI + Math.random() * Math.PI; // between 1.5pi and .5pi 
                    } else if (this.pos.x > 384)
                    {
                        this.angle = .5 * Math.PI + Math.random() * Math.PI; // between .5pi and 1.5pi 
                    } else
                    {
                        this.angle = 2 * Math.random() * Math.PI; //anywhere!
                    }

                }
                this.vel.x = Math.cos(this.angle);
                this.vel.y = Math.sin(this.angle);
                this.vel.y *= -1; //correct for cartesian vs screen coords
                this.vel.x *= SKA.CONST.COMPANION.SPEED * SKA.frames;
                this.vel.y *= SKA.CONST.COMPANION.SPEED * SKA.frames;
                if (Math.random() > .5) this.clockwise = true;

                //set animation variables
                if (this.vel.x >= 0)
                {
                    this.facingRight = true;
                } else
                {
                    this.facingRight = false;
                }

                if (this.vel.y <= 0)
                {
                    this.facingDown = false;
                } else
                {
                    this.facingDown = true;
                }

            }
        } else
        {
            //walking
            this.walkCooldown -= SKA.millis;
            //wander
            if (this.clockwise) this.angle += SKA.CONST.COMPANION.MAX_ROTATION * SKA.frames;
            else this.angle -= SKA.CONST.COMPANION.MAX_ROTATION * SKA.frames;
            this.vel.x = Math.cos(this.angle);
            this.vel.y = Math.sin(this.angle);
            this.vel.y *= -1; //correct for cartesian vs screen coords
            this.vel.x *= SKA.CONST.COMPANION.SPEED * SKA.frames;
            this.vel.y *= SKA.CONST.COMPANION.SPEED * SKA.frames;

            //set animation variables
            if (this.vel.x >= 0)
            {
                this.facingRight = true;
            } else
            {
                this.facingRight = false;
            }

            if (this.vel.y <= 0)
            {
                this.facingDown = false;
            } else
            {
                this.facingDown = true;
            }

            if (this.walkCooldown <= 0)
            {
                //start standing
                this.standCooldown = (.9 + .2 * Math.random()) * SKA.CONST.OTHER.STAND_COOLDOWN;
            }
        }

        //update movement
        this.updateMovement();

        //set animation
        if (this.vel.x !== 0 || this.vel.y !== 0)
        {
            //set to moving
            if (this.facingDown)
            {
                this.setAnimation('walk-d');
            } else
            {
                this.setAnimation('walk-u');
            }
        } else
        {
            //set to stand
            //if (this.facingDown)
            //{
            this.setAnimation('stand-d');
            //}
            //else
            //{
            //    this.setAnimation('stand-u');
            //}
        }

        this.flipX(!this.facingRight);

        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        //friendship circle
        if (this.friend)
        {
            context.beginPath();
            context.fillStyle = "rgba(200, 50, 20, .2)";
            //context.moveTo(this.pos.x + this.hWidth + 3, this.pos.y + 3 + this.hHeight);
            context.arc(this.pos.x + this.hWidth, this.pos.y + this.hHeight, 1.5 * this.width, 0, 2 * Math.PI);
            context.closePath();
            context.fill();
        }
        this.parent(context);
    }
});



////////////////////////////////////////////////////////////////////////////////////
//     HATER ENTITY                 ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
SKA.HaterEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = "hater";
        settings.spritewidth = 16;
        settings.spriteheight = 16;
        this.parent(x, y, settings);
        this.type = me.game.ENEMY_OBJECT;
        //this.renderable.resize(2);

        // set the walking & jumping speed
        //set to large number since i'll handle it
        this.setVelocity(100, 100);

        // adjust the bounding box
        //		this.updateColRect(8, 16, 0, 32);

        // top-down, no gravity
        this.collidable = true;
        this.jumping = false;
        this.falling = false;
        this.gravity = 0;
        this.alwaysUpdate = true;

        //states
        this.standCooldown = (.6 + .8 * Math.random()) * SKA.CONST.HATER.STAND_COOLDOWN;
        this.walkCooldown = 0;
        this.angle = 0;
        this.facingRight = true;
        this.facingDown = true;
        this.clockwise = false;
        this.friend = false;
        this.friendCooldown = 0;

        this.target = new me.Vector2d(0, 0);
        this.rand = 0;

        //animations
        this.renderable.addAnimation("stand-d", [0]);
        this.renderable.addAnimation("walk-d", [0, 1]);
        SKA.enemyList.push(this);
    },


    setAnimation: function (animation, restart)
    {
        if (restart)
        {
            //just set animation, this will restart it
            this.renderable.setCurrentAnimation(animation, animation);
            this.renderable.setAnimationFrame();
        } else
        {
            //only set if it is a different animation
            if (!this.renderable.isCurrentAnimation(animation))
            {
                this.renderable.setCurrentAnimation(animation, animation);
                this.renderable.setAnimationFrame();
            }
        }
    },

    addFriend: function ()
    {
        this.friend = true;
        this.friendCooldown = SKA.CONST.HATER.FRIEND_COOLDOWN;
    },

    readyToFriend: function ()
    {
        return (this.friendCooldown <= 0);
    },

    update: function ()
    {
        if (this.friendCooldown > 0)
        {
            this.friendCooldown -= SKA.millis;
            this.setAnimation('walk-d');
            this.vel.x = 0;
            this.vel.y = 0;
            this.parent(this);
            return true;
        }

        if (this.standCooldown > 0)
        {
            //standing
            this.standCooldown -= SKA.millis;
            this.vel.x = 0;
            this.vel.y = 0;
            if (this.standCooldown < 0)
            {
                //start walking
                me.audio.play('hater');
                this.walkCooldown = (.6 + .8 * Math.random()) * SKA.CONST.HATER.WALK_COOLDOWN;

                //pick one of three targets
                this.rand = Math.random();
                if (this.rand < .333)
                {
                    //1. player
                    this.target.x = SKA.player.pos.x;
                    this.target.y = SKA.player.pos.y;
                } else if (this.rand < .666)
                {
                    //2. companion
                    this.target.x = SKA.companion.pos.x;
                    this.target.y = SKA.companion.pos.y;
                } else
                {
                    //3. midpoint between player and connection
                    this.target.x = SKA.companion.pos.x +  .5 * (SKA.companion.pos.x - SKA.player.pos.x);
                    this.target.y = SKA.companion.pos.y +  .5 * (SKA.companion.pos.y - SKA.player.pos.y);
                }
            
                this.vel.x = this.target.x - this.pos.x;
                this.vel.y = this.target.y - this.pos.y;
                this.vel.normalize();
                this.vel.x *= SKA.CONST.HATER.SPEED * SKA.frames;
                this.vel.y *= SKA.CONST.HATER.SPEED * SKA.frames;

                //set animation variables
                if (this.vel.x >= 0)
                {
                    this.facingRight = true;
                } else
                {
                    this.facingRight = false;
                }
            }
        } else
        {
            //walking
            this.walkCooldown -= SKA.millis;
            //attack!
            this.vel.x = this.target.x - this.pos.x;
            this.vel.y = this.target.y - this.pos.y;
            this.vel.normalize();
            this.vel.x *= SKA.CONST.HATER.SPEED * SKA.frames;
            this.vel.y *= SKA.CONST.HATER.SPEED * SKA.frames;

            //set animation variables
            if (this.vel.x >= 0)
            {
                this.facingRight = true;
            } else
            {
                this.facingRight = false;
            }

            if (this.walkCooldown <= 0 || this.pos.distance(this.target) < SKA.CONST.HATER.SPEED)
            {
                //start standing
                this.standCooldown = (.6 + .8 * Math.random()) * SKA.CONST.HATER.STAND_COOLDOWN;
            }
        }

        //update movement
        this.updateMovement();

        //set animation
        if (this.vel.x !== 0 || this.vel.y !== 0)
        {
            //set to moving

            this.setAnimation('walk-d');
        } else
        {
            this.setAnimation('stand-d');
        }

        this.flipX(!this.facingRight);

        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        //friendship circle
        if (this.friend)
        {
            context.beginPath();
            context.fillStyle = "rgba(200, 50, 20, .2)";
            //context.moveTo(this.pos.x + this.hWidth + 3, this.pos.y + 3 + this.hHeight);
            context.arc(this.pos.x + this.hWidth, this.pos.y + this.hHeight, 1.5 * this.width, 0, 2 * Math.PI);
            context.closePath();
            context.fill();
        }
        this.parent(context);
    }
});

///////////////////////////////////////////////////////////
//  PARTICLE ENTITY
///////////////////////////////////////////////////////////
SKA.ParticleEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        var w, h, image;
        w = settings.spritewidth;
        h = settings.spriteheight;
        image = me.loader.getImage(settings.image);
        settings.spritewidth = image.width; //correct for when particles are bigger than  image
        settings.spriteheight = image.height;
        this.parent(x, y, settings);
        this.r = settings.r;
        this.g = settings.g;
        this.b = settings.b;
        this.a = 1;
        this.width = w;
        this.height = h;
        this.tweener = new me.Tween(this);
        this.time = 0;
        this.tweener.to({ time: 1 }, settings.duration);
        this.duration = settings.duration || 500;
        this.vel.x = settings.velX || 0;
        this.vel.y = settings.velY || 0;
        this.accel = new me.Vector2d(settings.accelX, settings.accelY);
        this.gravity = settings.gravity || 0;
        this.collidable = false;
        this.float = settings.float === true;
        this.speed = settings.speed || SKA.CONST.PARTICLE.FOLLOW_SPEED;

        this.follow = settings.follow;
        this.source = settings.source;

        this.lastViewport = me.game.viewport.pos.x;

        this.tweener.onComplete(this.destroy);
        this.tweener.start();
    },

    update: function ()
    {
        if (!this.follow)
        {
            //this.time should be updated by the tweener
            this.vel.x += this.accel.x;
            this.vel.y += this.accel.y;
            //this.renderable.setOpacity(1 - (.5 * this.time));
            //this.renderable.resize(1.5 - this.time);

            if (this.float &&
                me.game.viewport.pos.x !== this.lastViewport)
            {
                this.pos.x += me.game.viewport.pos.x - this.lastViewport;
                this.lastViewport = me.game.viewport.pos.x;
            }


            this.a = .7 - (.5 * this.time);
        } else //this.follow
        {
            if (this.pos.distance(this.follow.pos) <= this.speed)
            {
                this.destroy();
                return false;
            }

            //new way, follow a line between player and companion
            //this.pos.x = this.source.pos.x + this.source.hWidth + ((this.follow.pos.x + this.follow.hWidth - this.source.pos.x - this.source.hWidth) ) * this.time;
            //this.pos.y = this.source.pos.y + this.source.hHeight + ((this.follow.pos.y + this.follow.hHeight - this.source.pos.y - this.source.hHeight)) * this.time;

            //old way:
            this.vel.x = (this.follow.pos.x + this.follow.hWidth) - (this.pos.x + .5*this.width);
            this.vel.y = (this.follow.pos.y + this.follow.hHeight) - (this.pos.y + .5* this.height);
            this.vel.normalize();
            this.vel.x *= this.speed * SKA.frames;
            this.vel.y *= this.speed * SKA.frames;

            this.a = .3 - (.15 * this.time);
        }

        this.updateMovement();
        this.parent(this);
        return true;
    },

    draw: function (context)
    {
        if (!this.follow)
        {
            //context.globalOpacity = this.a;
            context.fillStyle = "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.a + ")";
            context.fillRect(this.pos.x, this.pos.y, this.width, this.height);
            //context.fillRect(this.pos.x + me.game.viewport.pos.x, this.pos.y + me.game.viewport.pos.y, this.width, this.height);
            //context.globalOpacity = 1;
        } else
        {
            context.beginPath();
            context.arc(this.pos.x + .5*this.width, this.pos.y + .5*this.height, .5*this.width, 0, 2 * Math.PI, false);
            context.fillStyle = "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.a + ")";
            context.fill();
        }
    },

    destroy: function ()
    {
        this.alive = false;
        me.game.remove(this);
    },
});

SKA.spawnParticles = function (amount, dir, spread, x, y, settings)
{

    var unit, i, angle, part, size, vel, baseSize, red, green, blue, dur, variance, baseVel, baseDur, grav, dirVec;
    baseVel = settings.vel || 3.2;
    baseSize = settings.size || 20;
    red = settings.r || 245;
    green = settings.g || 240;
    blue = settings.b || 86;
    variance = settings.variance || 1;
    baseDur = settings.duration || 500;
    grav = settings.gravity || .1;
    dirVec = new me.Vector2d(dir.x, dir.y);
    dirVec.normalize();

    unit = new me.Vector2d(0, 0);
    for (i = 0; i < amount; i++)
    {
        angle = unit.angle(dirVec);
        angle += (.5 * spread) - (spread * Math.random());
        size = 1 + (variance * Math.random());
        vel = baseVel * (2 * (variance * Math.random()));
        dur = baseDur * (1 + (variance * Math.random()));
        part = me.entityPool.newInstanceOf('particle', x, y, {
            image: 'particle', spritewidth: baseSize * (size), spriteheight: baseSize * (size),
            velX: vel * Math.cos(angle), velY: vel * Math.sin(angle),
            accelX: 0, accelY: 0,
            duration: dur,
            r: red, g: green, b: blue,
            gravity: grav,
            float: settings.float
        });
        me.game.add(part, settings.layer);
    }
    me.game.sort();
};

////////////////////////////////////////////////////////////////////////////////////
//        COLLECTABLE ENTITY        ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
SKA.CollectableEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        settings.image = settings.image || settings.name || 'Coin';
        this.image = settings.image;
        settings.spriteheight = 16;
        settings.spritewidth = 16;

        this.parent(x, y, settings);
        this.type = me.game.COLLECTABLE_ENTITY;
        this.score = settings.score || 0;
        this.label = settings.label || this.image;
        SKA.collectableCount += 1;
        this.alwaysUpdate = true;
    },

    update: function ()
    {
        this.parent(this);
        return true;
    },

    onCollision: function ()
    {
        // do something when collected
        this.collidable = false;
        this.alive = false;
        SKA.collectableCount -= 1;

        //add score
        SKA.player.addScore(this.score);
        SKA.player.setPickup(this.image, this.label);

        me.audio.play("pickup");
        me.game.remove(this);
    }
}
);

////////////////////////////////////////////////////////////////////////////////////
//        UTILITY                   ////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//from lemothe's 'tricks of the windows game programming gurus'
SKA.checkLineSegmentCollision = function (a_x, a_y, b_x, b_y, p_x, p_y, q_x, q_y)
{
    var s1x, s1y, s2x, s2y, s, t;
    //parameterize
    s1x = b_x - a_x;
    s1y = b_y - a_y;
    s2x = q_x - p_x;
    s2y = q_y - p_y;

    //find parameters for meeting
    s = (-s1y * (a_x - p_x) + s1x * (a_y - p_y)) / (-s2x * s1y + s1x * s2y);
    t = (s2x * (a_y - p_y) - s2y * (a_x - p_x)) / (-s2x * s1y + s1x * s2y);

    //collision if parameters are between 0 and 1
    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        return true;
    }
    return false;
}